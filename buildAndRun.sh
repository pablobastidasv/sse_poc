#!/bin/sh
mvn clean package && docker build -t co.kopenhagen/sse_poc .
docker rm -f sse_poc || true && docker run -d -p 8080:8080 -p 4848:4848 --name sse_poc co.kopenhagen/sse_poc 
