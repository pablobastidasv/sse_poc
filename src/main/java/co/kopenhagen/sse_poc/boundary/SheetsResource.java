package co.kopenhagen.sse_poc.boundary;

import javax.annotation.PostConstruct;
import javax.ejb.Singleton;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.Path;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Context;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.sse.OutboundSseEvent;
import javax.ws.rs.sse.Sse;
import javax.ws.rs.sse.SseBroadcaster;
import javax.ws.rs.sse.SseEventSink;
import java.util.UUID;

/**
 *
 * @author airhacks.com
 */
@Path("broadcast")
@Singleton
public class SheetsResource {

    @Context
    private Sse sse;

    private SseBroadcaster broadcaster;

    @PostConstruct
    public void initializer(){
        broadcaster = sse.newBroadcaster();
    }

    @GET
    @Path("subscribe")
    @Produces(MediaType.SERVER_SENT_EVENTS)
    public void register(@Context SseEventSink sink){
        sink.send(sse.newEvent("You are subscribed"));
        broadcaster.register(sink);
    }

    @POST
    public void save(){
        OutboundSseEvent event = sse.newEventBuilder()
                .id(UUID.randomUUID().toString())
                .data("the time is " + System.currentTimeMillis())
                .name("Creation")
                .comment("Hi hi hi")
                .build();
        broadcaster.broadcast(event);
    }
}
